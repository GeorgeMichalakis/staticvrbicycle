using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelRotation : MonoBehaviour
{
    float rotation = 0f;
    void Update()
    {
        rotation -= 5f;
        transform.eulerAngles = new Vector3(0f, 0f, rotation);
    }
}
