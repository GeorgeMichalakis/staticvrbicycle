using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    float speed;
    // Start is called before the first frame update
    void Start()
    {
      speed = Random.Range(0.4f, 0.6f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += new Vector3(speed, 0f,0f);
    }

}
