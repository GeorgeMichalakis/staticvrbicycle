using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class ArduinoReader : MonoBehaviour
{

    SerialPort arduinoStream = new SerialPort("COM12", 9600);

    void Start()
    {
        arduinoStream.Open();
    }

    // Update is called once per frame
    void Update()
    {
        string value = arduinoStream.ReadLine();
        print(int.Parse(value));
    }
}
